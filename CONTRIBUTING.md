# Fenix Installer - Reborn OS
*Documentation by Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)*

![](media/screenshots/1.png)

## Overview

### Introduction
Fenix Installer is written in Python and uses python bindings for Gtk for the user interface (UI). The UI is designed in [Glade](https://glade.gnome.org/).

### Directory Structure
The project directory consists of five folders:

|        | Folder       | Description                                                        |
| :----- | :------------| :------------------------------------------------------------------|
| 1      | config       | files that store data about the system and the installer status    |
| 2      | forms        | UI files from Glade                                                |
| 3      | log          | log files                                                          |
| 4      | media        | multimedia files                                                   |
| 5      | pages        | python scripts corresponding to each installer "page"              |
| 6      | scripts      | python or bash helper scripts that perform the installer functions |

The directory structure looks like this:
```
fenix-installer/  
              |
              |-config/  
              |   |-installer.json  
              |   |-system.json  
              |   |-resources.json
              |
              |-forms/  
              |   |-main.glade  
              |   |-welcome.glade  
              |   |-introduction.glade  
              |   |-...
              |   ...
              |  
              |-pages/
              |   |-welcome.py
              |   |-introduction.py
              |   |-...
              |   ...
              |
              |-scripts/
              |   |-fenix-utilities.py
              |   |-test.sh
              |   |-install.sh
              |
              |-media/    
              |   |-branding/
              |   |        |-Reborn_OS_Logo_1.svg
              |   |        |-Reborn_OS_Logo_2.svg
              |   |
              |   |-screenshots/
              |   |        |-1.png
              |   |        ...
              |   ...
              |
              |-log/  
              |   |-2019-06-05_055137_CDT.log
              |   |-2019-06-05_054130_CDT.log
              |   ...
              |   
              |-fenix-installer.py
              |
              |-LICENSE
              |
              |-README.md
              |
              |-CONTRIBUTING.md
```

## Development

### Adding a page
There are three parts to adding a page:
1. Design the UI in Glade
2. Code the page in Python
3. Edit the configuration file

#### Part I.   Adding a page: Design the UI in Glade
**Note: Make sure that the main container is `Gtk.Box` and it houses everything else inside it**
1. Design the user interface in `Glade` by following a tutorial like [this](http://www.holmes4.com/wda/MyBooks/PythonGTK/PyGTK_Development-Dynamic_User_Interfaces-Glade.html),  **for one page** in [Glade](https://glade.gnome.org/) and save the `.glade` file in `/forms/`.
2. Assign handler names for the signals that you will handle, corresponding to any user interaction. Check [this link](http://www.holmes4.com/wda/MyBooks/PythonGTK/PyGTK_Development-Dynamic_User_Interfaces-Glade.html) again and scroll down to the section on **Signals**.
3. Save the file with a ".glade" extension (for example `partitioning.glade`) in the folder "forms".

#### Part II.  Adding a page: Code the page in Python

1. Make a copy of an existing page, for example `/pages/welcome.py` in the same folder and rename the duplicate to the same name as the glade file in Part I while preserving the .py extension (for example `partitioning.py`)

2. Change the *first* demarcated section in the Python page file to modify the name of the class. For example:

    > In **pages/partitioning.py**
    ```python
    # ----------- Modify this ---------- #
    CURRENT_PAGE_NAME = "welcome"
    # ----------- Modify this ---------- #
    ```
    can become
    
    > In **pages/partitioning.py**
    ```python
    # ----------- Modify this ---------- #
    CURRENT_PAGE_NAME = "partitioning"
    # ----------- Modify this ---------- #
    ```

3. For readability, sections are demarcated for custom code  
    a. `# CUSTOM IMPORTS` section for external dependencies  
    b. `Custom code` section in the `__init__` *method* for code to be run once in the beginning:  
    ```python
    # ---------- Custom code ----------- #
    # -------- Custom code ends -------- #
    ```
    c. The `# EVENT HANDLERS` sectio, can define user event handlers (the names of which should already be set in the **Signals** tab on `Glade`)
    d. `# CUSTOM METHODS` section for *methods* (or *functions*)

#### Part III. Adding a page: Edit the configuration file

In `installer.json`, populate both "pages" and "added_pages"

For example:  
> In **config/installer.json**
```json
{
    "pages": {
        "welcome": {
            "file": "welcome",
            "Gtk_ID": "welcome",
            "title": "Welcome"
        },
        "introduction": {
            "file": "introduction",
            "Gtk_ID": "introduction",
            "title": "Introduction"
        },
        "mode": {
            "file": "mode",
            "Gtk_ID": "mode",
            "title": "Mode"
        }
    },
    "initial_pages": [
        "welcome",
        "introduction",
        "mode"
    ],
    "added_pages": [
    ]
}
```
can become

> In **config/installer.json**
```json
{
    "pages": {
        "welcome": {
            "file": "welcome",
            "Gtk_ID": "welcome",
            "title": "Welcome"
        },
        "introduction": {
            "file": "introduction",
            "Gtk_ID": "introduction",
            "title": "Introduction"
        },
        "mode": {
            "file": "mode",
            "Gtk_ID": "mode",
            "title": "Mode"
        },
        "partition": {
            "file": "partition",
            "Gtk_ID": "partition",
            "title": "Partition"
        }
    },
    "initial_pages": [
        "welcome",
        "introduction",
        "mode"
    ],
    "added_pages": [
        "partition"
    ]
}
```

In the above ".json" file, 
1. **"file"** refers to the names of the .glade and .py files created for the installer. If you used "partitioning.glade" and "partitioning.py", then "file" will be "partitioning".
2. **"Gtk_ID"** refers to the ID assigned to the top-level Gtk.Box that you created in Part I to house all the UI. If you do not know it yet, assign an ID for it in the right hand side panel of Glade after opening the .glade file for this page.
3. **"title"** refers to the title that shows up for this page in the installer.