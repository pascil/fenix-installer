# FENIX INSTALLER
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# IMPORTS
from scripts.fenix_utilities import InstallerPage, ImageTools, ConfigTools, TextTools

# CUSTOM IMPORTS
import json                                              # for reading config files
import gi                                                #
gi.require_version('Gtk', '3.0')                         #
from gi.repository import Gtk, Pango                     # for text tags
# from gi.repository import WebKit2, WebKit2WebExtension

# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "introduction"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self)                             # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        self.addSmallLogo()
        self.addIntroduction()                   
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    

    # CUSTOM METHODS
    def addSmallLogo(self):
        resources = ConfigTools.getConfigData("resources.json")                                
        ImageTools.addImageFromFile(resources["smallLogo"], 50, self.builder.get_object("smallLogo")) # add a small logo image

    def addIntroduction(self):
        introductionTextBuffer = self.builder.get_object("introduction_TextView").get_buffer()
        TextTools.addBasicTextTags(introductionTextBuffer)
        iter = introductionTextBuffer.get_start_iter()
        introductionTextBuffer.insert_with_tags_by_name(iter, "Choose From Over 10 Desktop Environments:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nOne ISO with unlimited Desktop Environments to choose from! Whether you are a fan of KDE, GNOME, Deepin, Budgie, OpenBox, i3, or Xfce – Reborn OS is for you. For those who love the terminal, there is even the option of a Base install, allowing you to bypass the Desktop Environment entirely. Reborn OS gets out of the way and lets you decide, not the other way around.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nCustomize it to your heart’s content", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nWith about 20 different options in the installer, Reborn OS truly caters to your needs. Whatever they may be.")               
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nNever Fall Behind:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nReborn OS is based on both Arch Linux and Antergos, meaning that it features a rolling release model. Due to this, you will only ever have to install once – and never again. Enjoy getting the latest and the best from the Linux community, all available in an easy-to-use software center.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nPrivate and Secure:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nUnlike Microsoft and Mac OS, Reborn OS does not collect any user data on anyone. That means that you can feel completely safe using Reborn, knowing that your activities are not being recorded by anyone. In fact, Linux, the software Reborn OS runs on, is the same software that powers the U.S. Department of Defense!")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nBuilt For You – By You:", "bold")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\nReborn OS truly puts our users first, implementing your suggestions in each new release.") 
