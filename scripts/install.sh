#! /usr/bin/bash

# FENIX INSTALLER
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# Set the system clock
timedatectl set-ntp true

# Create partitions and write the changes
(
echo o
echo Y
echo n
echo 1
echo 2048
echo +512M
echo 8300
echo n
echo 2
echo 
echo +1M
echo EF02
echo n
echo 3
echo 
echo 
echo 8300
echo w
echo Y
) | gdisk /dev/sda

# Format the partition and create an ext4 file system
mkfs.ext4 /dev/sda1
mkfs.ext4 /dev/sda3

# Mount the partitions
mount /dev/sda3 /mnt
cd /mnt
mkdir boot
mount /dev/sda1 boot

# Install the base package
pacstrap /mnt base grub

# Generate an fstab file
genfstab -L /mnt >> /mnt/etc/fstab

# Change into the installation root
cat << EOF | arch-chroot /mnt
    # Set timezone and locale
    ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime
    hwclock --systohc --utc
    locale-gen
    echo LANG=en_US.UTF-8 > /etc/locale.conf
    export LANG=en_US.UTF-8

    # Set hostname
    echo ArchLinuxFenixTest > /etc/hostname

    systemctl enable dhcpcd
EOF
cat << EOF | arch-chroot /mnt
    # Install bootloader
    pacman -S grub os-prober
    Y
EOF
cat << EOF | arch-chroot /mnt
    grub-install /dev/sda
EOF
cat << EOF | arch-chroot /mnt
    grub-mkconfig -o /boot/grub/grub.cfg
EOF