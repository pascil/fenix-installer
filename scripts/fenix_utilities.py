# FENIX INSTALLER
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# IMPORTS
import datetime # for time stamps
import importlib # for dynamic imports                                    
import os # for deleting files
import threading # for multithreading
import subprocess # for running commands and scripts
import json # for reading config files
import gi # Gtk related modules
gi.require_version('Gtk', '3.0')   
from gi.repository import Gtk, GdkPixbuf, GLib, Pango

class InstallerPage(Gtk.Box):
# Create a box to host the GUI elements
    # CONSTRUCTOR
    def __init__(self, currentPageName, mainBuilder):
        Gtk.Box.__init__(self)                                                         # call the super-class constructor        
        self.mainBuilder                  = mainBuilder                                # get access to the UI of the main window
        self.consoleBuffer                = mainBuilder.get_object("consoleTextView").get_buffer() # get access to the provided console buffer
        self.__formFilePath, self.__GtkID = self.__getPageInfo(currentPageName)        # read configuration files to find out information about the page
        self.builder                      = self.__getFormObjects(self.__formFilePath) # get form objects (in the form of a Gtk builder) from the form file
        self.__addFormObjects(self.builder, self.__GtkID)                              # add the form objects to the current page's container
        self.__trapString = "trap \'printf \"[%s %s %s] %s\\n\" $(date \'\"\'\"\'+%F, %T %Z\'\"\'\"\') \"$BASH_COMMAND\"\' DEBUG" # bash trap command to display inputs and outputs with time stamps. Multiple nested quotes have been escaped.
        self.threads = []                                                              # starting with an empty pool of threads
        self.logFile = []                                                              # no log file created initially

    @staticmethod
    def __getPageInfo(currentPageName):    
        with open("config" + "/" + "installer.json", "r") as installer_config_file:  # read the installer configuration file                  
            installer_data = json.load(installer_config_file)                        #                                        
            formFilePath = "forms" + "/" + installer_data["pages"][currentPageName]["file"] + "." + "glade" # get the UI file path for the current page
            GtkID       = installer_data["pages"][currentPageName]["Gtk_ID"]                                # get the Gtk ID for the UI file for the top-level container in this page
            return formFilePath, GtkID

    @staticmethod
    def __getFormObjects(formFilePath):
        builder = Gtk.Builder()             # 
        builder.add_from_file(formFilePath) # create Gtk objects from the UI file
        return builder

    def __addFormObjects(self, builder, containerID):
        page = builder.get_object(containerID) # get the Gtk container for this page
        self.pack_start(page, True, True, 0)   # add the extracted GUI elements to the current box  

    def appendToConsoleText(self, text):
        self.consoleBuffer.insert(self.consoleBuffer.get_end_iter(), text) # update the GUI console with additional text

    @staticmethod
    def getTimeStamp():
        return "[" + datetime.datetime.now().strftime("%Y-%m-%d, %H:%M:%S ") + datetime.datetime.now(datetime.timezone.utc).astimezone().tzname() + "]"

    def waitForCommandsToFinish(self):
        for thread in self.threads:           #
            thread.join()                     # wait for all running threads to finish

    def openLogFile(self):
        fileName = "log" + "/" + datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S") + "_" + datetime.datetime.now(datetime.timezone.utc).astimezone().tzname() + ".log"
        self.logFile = open(fileName, mode='a', buffering=1)

    def closeLogFile(self):
        self.logFile.close()

    def runShellScript(self, scriptPath):
        thread = threading.Thread(target=self.__shellScriptWorker, args = (scriptPath,)) # create a new thread and pass the script path to it
        self.threads.append(thread)                                                     # add the thread to the pool of threads
        thread.start()                                                                  # start the thread

    def runShellCommand(self, commandString):
        thread = threading.Thread(target=self.__shellCommandWorker, args = (commandString,)) # create a new thread and pass the command string to it
        self.threads.append(thread)                                                         # add the thread to the pool of threads
        thread.start()                                                                      # start the thread

    def __shellScriptWorker(self, scriptPath):
        with open(scriptPath + "_temp", "w") as script:
            with open(scriptPath, "r") as script_old:
                script.write(self.__trapString + "\n\n") # create a temporary script with a trap command to display both inputs and outputs, with time stamps              
                script.write(script_old.read())          # copy all lines from the script to a temporary script
        
        process = subprocess.Popen("sh " + scriptPath + "_temp", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) # run the temporary script (with a trap command) such that both the output and errors go to stdout
        while True:                        
            output = str(process.stdout.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None:
                break                                                     # command completed
            if output:                                                  
                self.logFile.write(output + "\n")                         # the output is written to the log file
                GLib.idle_add(self.appendToConsoleText, output + "\n")      # the output as it comes is added to the GUI console  
        
        os.remove(scriptPath + "_temp") # remove the temporary script

    def __shellCommandWorker(self, commandString):    
        input = self.getTimeStamp() + " " +  commandString
        self.logFile.write(input + "\n")                    # the input is written to the log file
        GLib.idle_add(self.appendToConsoleText, input + "\n") # the input is written to the console
        process = subprocess.Popen(commandString, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) # run the command such that both the output and errors go to stdout
        while True:                        
            output = str(process.stdout.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None:
                break                                                     # command completed
            if output:                   
                self.logFile.write(output + "\n")                         # the output is written to the log file      
                GLib.idle_add(self.appendToConsoleText, output + "\n")      # the output as it comes is added to the GUI console       

class ImageTools():
    @staticmethod
    def getPixbufFromFile(filePath):
        return GdkPixbuf.Pixbuf.new_from_file(filePath) # get pixbuf from an image in `filePath`

    @staticmethod
    def scalePixbufByHeight(pixbuf, desiredHeight, aspectRatio):
        return pixbuf.scale_simple(           # scale a given pixbuf to a specified height            
                aspectRatio * desiredHeight,
                desiredHeight,
                GdkPixbuf.InterpType.BILINEAR
            )

    @staticmethod
    def scalePixbufByWidth(pixbuf, desiredWidth, aspectRatio):
        return pixbuf.scale_simple(           # scale a given pixbuf to a specified width
                desiredWidth,                              
                desiredWidth / aspectRatio,
                GdkPixbuf.InterpType.BILINEAR
            )    

    @staticmethod
    def addImageFromPixbuf(pixbuf, gtkImage):
        GLib.idle_add(gtkImage.set_from_pixbuf, pixbuf)               # add pixbuf to GtkImage in the GUI

    @staticmethod
    def addImageFromFile(filePath, desiredHeight, gtkImage):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(filePath)             # load image from file
        aspectRatio = float(pixbuf.get_width()) / pixbuf.get_height() # calculate the aspect ratio of the image
        pixbuf = pixbuf.scale_simple(                                 # scale the image
                aspectRatio * desiredHeight,
                desiredHeight,
                GdkPixbuf.InterpType.BILINEAR
            )
        GLib.idle_add(gtkImage.set_from_pixbuf, pixbuf)               # add the scaled image to GtkImage in the GUI

class ConfigTools():
    @staticmethod
    def getConfigData(configFileName):
        with open("config" + "/" + configFileName, "r") as config_file: # read the configuration file
            configData = json.load(config_file)                         # 
            return configData                                           # return the extracted configuration data

    @staticmethod
    def setConfigData(configData, configFileName):
        with open("config" + "/" + configFileName, "w+") as config_file: # open the installer configuration file in write mode                                        
            config_file.write(json.dumps(configData, indent=4))  
    
class TextTools():
    @staticmethod
    def addBasicTextTags(textBuffer):
        textBuffer.create_tag("bold"     , weight=Pango.Weight.BOLD)         # create a text tag for bold text
        textBuffer.create_tag("italic"   , style=Pango.Style.ITALIC)         # create a text tag for italic text
        textBuffer.create_tag("underline", underline=Pango.Underline.SINGLE) # create a text tag for underlined text

class PageTools():
    @staticmethod
    def addPages(listOfPages, pageStack, mainBuilder, installer_data):
        for pageName in listOfPages:
            mod = importlib.import_module("pages" + "." + installer_data["pages"][pageName]["file"]) # dynamically import the necessary python file for the page
            page = mod.Page(mainBuilder) # use the corresponding python script of the page to initialize it
            page.set_name(pageName) # name the page for later access
            pageStack.add_titled(page, pageName, installer_data["pages"][pageName]["title"]) # add the imported page to the Gtk stack
        installer_data["added_pages"] = listOfPages
        return installer_data

    @staticmethod
    def removePages(listOfPages, pageStack, installer_data):     
        for page in pageStack.get_children():
            if page.get_name() in listOfPages:
                pageStack.remove(page)
        installer_data["added_pages"] = [pageName for pageName in installer_data["added_pages"] if pageName not in listOfPages]
        return installer_data

    @staticmethod
    def refreshPages(pageStack):
        pageStack.show_all()


    
