#! /usr/bin/python

# FENIX INSTALLER
# Please refer to the file `LICENSE` in the current directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# IMPORTS
import os
import importlib                 # for import automation
import gi                        # Gtk related modules
gi.require_version('Gtk', '3.0') 
from gi.repository import Gtk
from scripts.fenix_utilities import ConfigTools, PageTools

# THE EVENT HANDLER
class MainFormHandler:   
# Specify how this particular Gtk container handles user interaction events
# The names of handler functions (also called `signals` in Gtk) can be assigned in `Glade` under "Signals"

    def main_onClose (self, mainForm):
        # Called when the application is closed
        # This happens either when
        # (1) the container object is destroyed or 
        # (2) when the `Abort` button is clicked
        Gtk.main_quit()

    def main_onBackClicked (self, pageStack):
        # Called when either 
        # (1) the `Back` button is clicked or
        # (2) the `back icon` of the headerbar is clicked

        installer_data  = ConfigTools.getConfigData("installer.json")
        listOfPageNames = installer_data["initial_pages"] + installer_data["added_pages"]
        isInstalling    = installer_data["installing"]
        if not isInstalling:
            currentPageIndex = listOfPageNames.index(pageStack.get_visible_child_name()) # Determine the index of the current visible page
            if currentPageIndex > 0: # If the currently visible page is not the first one
                pageStack.set_visible_child_name(listOfPageNames[currentPageIndex - 1]) # Select the previous page name from the list and make it visible

    def main_onNextClicked (self, pageStack): 
        # Called when either 
        # (1) the `Next` button is clicked or
        # (2) the `Next icon` of the headerbar is clicked    
        
        installer_data  = ConfigTools.getConfigData("installer.json")
        listOfPageNames = installer_data["initial_pages"] + installer_data["added_pages"]
        isInstalling    = installer_data["installing"]
        if not isInstalling:
            currentPageIndex = listOfPageNames.index(pageStack.get_visible_child_name()) # Determine the index of the current visible page
            if currentPageIndex < (len(listOfPageNames) - 1): # If the currently visible page is not the last one
                pageStack.set_visible_child_name(listOfPageNames[currentPageIndex + 1])  # Select the next page name from the list and make it visible

    def main_consoleResized(self, consoleScolledWindow, rectangle):                  # when the console changes in size because of new text
        adjustment = consoleScolledWindow.get_vadjustment()                          #
        adjustment.set_value(adjustment.get_upper() - adjustment.get_page_size())    #
        consoleScolledWindow.set_vadjustment(adjustment)                             # scroll to the bottom of the console

# THE MAIN FUNCTION
def main():
    currentWorkingDirectory = os.path.dirname(os.path.realpath(__file__))               # get the parent directory and make that the current working directory
    os.chdir(currentWorkingDirectory)    

    system_data = ConfigTools.getConfigData("system.json")
    system_data["currentWorkingDirectory"] = currentWorkingDirectory
    ConfigTools.setConfigData(system_data, "system.json")                             

    builder = Gtk.Builder()                                                             # extract the main form from the glade file
    builder.add_from_file("forms" + "/" + "fenix-installer" + ".glade")
    builder.connect_signals(MainFormHandler())                                          # connect the signals from the Gtk forms to our event handlers (which are all defined in a class)
    pageStack = builder.get_object("fenix-installer.box.paned.stack")                   # get the installer's Gtk Stack. This stack stores the "pages" of our installer and shows them one at a time

    default_mode = "semi-automatic"
    installer_data = ConfigTools.getConfigData("installer.json")
    listOfPages = installer_data["initial_pages"] + installer_data["page_templates"][default_mode]
    installer_data["mode"] = default_mode
    installer_data = PageTools.addPages(listOfPages, pageStack, builder, installer_data)
    installer_data["added_pages"] = installer_data["page_templates"][default_mode] # override the action of addPages adding initial pages like "Welcome", "Introduction" and "Mode" to this variable
    ConfigTools.setConfigData(installer_data, "installer.json")
   
    builder.get_object("fenix-installer").show_all() # get the main form object and make it visible
    Gtk.main() # start the GUI event loop 
    
# THE EXECUTION STARTS HERE
if __name__ == '__main__':
    main()